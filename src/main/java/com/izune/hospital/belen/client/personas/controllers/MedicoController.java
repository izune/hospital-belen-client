/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.personas.controllers;

import com.izune.hospital.belen.client.config.Resource;
import com.izune.hospital.belen.client.personas.wrappers.MedicoWrapper;
import com.izune.isabel.net.RestClient;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author aneyra
 */
@Controller
public class MedicoController {

    @Autowired
    private RestClient<MedicoWrapper[]> listMedico;

    @Autowired
    private RestClient<MedicoWrapper> medico;

    @Autowired
    private RestClient<Object> rest;

    @Autowired
    private RestClient<Boolean> existe;

    public List<MedicoWrapper> listaMedicos() throws HttpClientErrorException {

        return Arrays.asList(listMedico.authorization(Resource.TOKEN)
                .resource("/personas/medico")
                .clazz(MedicoWrapper[].class).get().build());

    }

    public boolean grabar(MedicoWrapper medicoWrapper) throws HttpClientErrorException {

        rest.resource("/personas/medico")
                .authorization(Resource.TOKEN).body(medicoWrapper);
        if (0 == medicoWrapper.getId()) {
            rest.post().build();
        } else {
            rest.put().build();
        }

        return true;
    }

    public MedicoWrapper obtenetMedico(long id) throws HttpClientErrorException {
        return medico
                .resource("/personas/medico").resourceId(id)
                .authorization(Resource.TOKEN)
                .clazz(MedicoWrapper.class).get().build();

    }

    public boolean existeCmp(String cmp, long excluir) throws HttpClientErrorException {

        return existe.resource("/personas/medico/search")
                .authorization(Resource.TOKEN)
                .param("cmp", cmp)
                .param("excluir", excluir)
                .clazz(Boolean.class).get().build();

    }

}
