/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.utils.views;

import com.izune.hospital.belen.client.utils.controllers.MainController;
import com.izune.hospital.belen.client.utils.wrappers.MenuWrapper;
import com.izune.isabel.interfaces.IFormParams;
import com.izune.isabel.interfaces.IListUI;
import com.izune.isabel.ui.ReportUI;

import java.util.List;
import javax.swing.JInternalFrame;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author aneyra
 */
@Component
public class MainUI extends javax.swing.JFrame {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private MainController mainController;
    private List<MenuWrapper> menuList;

    public MainUI() {
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jSplitPane1 = new javax.swing.JSplitPane();
        desktopPane = new javax.swing.JDesktopPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        tree = new javax.swing.JTree();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exitMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jToolBar1.setRollover(true);

        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setDividerSize(5);

        javax.swing.GroupLayout desktopPaneLayout = new javax.swing.GroupLayout(desktopPane);
        desktopPane.setLayout(desktopPaneLayout);
        desktopPaneLayout.setHorizontalGroup(
            desktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 289, Short.MAX_VALUE)
        );
        desktopPaneLayout.setVerticalGroup(
            desktopPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 246, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(desktopPane);

        tree.setRootVisible(false);
        tree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                treeMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tree);

        jSplitPane1.setLeftComponent(jScrollPane2);

        fileMenu.setMnemonic('f');
        fileMenu.setText("Archivo");

        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Salir");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jSplitPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        menuList = mainController.getMenu();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Menu");
        cargarMenu(root, 0);
        DefaultTreeModel model = new DefaultTreeModel(root);
        tree.setModel(model);
        tree.expandRow(0);
        tree.expandRow(5);
        tree.expandRow(2);
    }//GEN-LAST:event_formWindowOpened

    private void treeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeMousePressed
        try {
            if (null == tree.getLastSelectedPathComponent()) {
                return;
            }

            if (2 == evt.getClickCount()) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                MenuWrapper menu = (MenuWrapper) node.getUserObject();

                if (null != menu.getUi()) {
                    if (menu.getUi().endsWith("JR")) {
                        IFormParams frame = (IFormParams) context.getBean(menu.getUi());
                        frame.setVisible(true);

                        JInternalFrame report = new ReportUI().getReport(frame.getData(), "citasPorEspecialidad.jasper", menu.getNombre());
                        desktopPane.add(report);
                        report.setVisible(true);
                    } else {
                        IListUI frame = (IListUI) context.getBean(menu.getUi());
                        frame.setDesktopPane(desktopPane);
                        desktopPane.add((java.awt.Component) frame);
                        frame.setVisible(true);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_treeMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JTree tree;
    // End of variables declaration//GEN-END:variables

    public void cargarMenu(DefaultMutableTreeNode root, int id) {
        for (MenuWrapper menuWrapper : menuList) {
            if (menuWrapper.getMenu() == id) {
                DefaultMutableTreeNode child = new DefaultMutableTreeNode();
                child.setUserObject(menuWrapper);
                cargarMenu(child, menuWrapper.getMenuId());
                root.add(child);
            }
        }
    }
}
