/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

import com.izune.isabel.Isabel;
import com.izune.isabel.annotation.table.CellEditable;
import com.izune.isabel.annotation.table.Column;
import com.izune.isabel.annotation.table.Format;

/**
 *
 * @author aneyra
 */
public class CronogramaDetalleWrapper {

    @Column(name = "Especialidad")
    private String especialidad;

    @Column(name = "Medico")
    private String medico;

    @Column(name = "Nro Citas")
    @Format(constant = Isabel.NUMBER)
    @CellEditable
    private int limiteCitas;

    @Column(visible = false)
    private long id;

    @Column(visible = false)
    private int item;

    @Column(visible = false)
    private long personaId;

    @Column(visible = false)
    private int especialidadId;

    public CronogramaDetalleWrapper() {
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public int getLimiteCitas() {
        return limiteCitas;
    }

    public void setLimiteCitas(int limiteCitas) {
        this.limiteCitas = limiteCitas;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(long personaId) {
        this.personaId = personaId;
    }

    public int getEspecialidadId() {
        return especialidadId;
    }

    public void setEspecialidadId(int especialidadId) {
        this.especialidadId = especialidadId;
    }

}
