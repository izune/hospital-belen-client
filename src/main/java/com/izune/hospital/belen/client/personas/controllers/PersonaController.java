/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.personas.controllers;

import com.izune.hospital.belen.client.config.Resource;
import com.izune.hospital.belen.client.personas.wrappers.DocumentoIdentidadWrapper;
import com.izune.isabel.net.RestClient;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author aneyra
 */
@Controller
public class PersonaController {

    @Autowired
    private RestClient<Long> existePersona;

    @Autowired
    private RestClient<DocumentoIdentidadWrapper[]> listTipoDocumento;

    public Long existeNumeroDocumento(String documento, long excluir) throws HttpClientErrorException {

        return existePersona.resource("/personas/search")
                .authorization(Resource.TOKEN)
                .param("documento", documento)
                .param("excluir", excluir)
                .clazz(Long.class).get().build();

    }

    public List<DocumentoIdentidadWrapper> listarDocumentoIdentidad() throws HttpClientErrorException {
        return Arrays.asList(listTipoDocumento.authorization(Resource.TOKEN)
                .resource("/personas/documento-identidad").clazz(DocumentoIdentidadWrapper[].class)
                .get().build());

    }
}
