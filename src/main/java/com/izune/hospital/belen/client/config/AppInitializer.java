package com.izune.hospital.belen.client.config;

import com.izune.hospital.belen.client.usuarios.views.LoginUI;
import javax.swing.UIManager;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppInitializer {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(new NimbusLookAndFeel());
        } catch (Exception e) {

        }
        
        System.out.println(System.getProperty("user.dir"));

        @SuppressWarnings("resource")
        AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext();
        app.register(AppConfig.class);
        app.refresh();

        LoginUI loginUI = app.getBean(LoginUI.class);

        loginUI.setVisible(true);

    }

}
