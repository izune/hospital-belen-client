package com.izune.hospital.belen.client.citas.views;

import com.izune.hospital.belen.client.citas.controllers.ProcedenciaController;
import com.izune.hospital.belen.client.citas.wrappers.DistritoWrapper;
import com.izune.hospital.belen.client.citas.wrappers.ProcedenciaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.TipoProcedenciaWrapper;
import com.izune.hospital.belen.client.utils.controllers.UtilController;
import com.izune.hospital.belen.client.utils.views.SearchUI;
import com.izune.isabel.Isabel;
import com.izune.isabel.exceptions.InvalidTextFormat;
import com.izune.isabel.exceptions.InvalidTypeColumn;
import com.izune.isabel.exceptions.NotFoundColumnAnnotation;
import com.izune.isabel.exceptions.NotFoundDisplayValueAnnotation;
import com.izune.isabel.interfaces.IMaster;
import com.izune.isabel.utils.Message;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

@Component
public class ProcedenciaUI extends javax.swing.JDialog implements IMaster<ProcedenciaWrapper> {

    private static final long serialVersionUID = 2008226891673663233L;

    @Autowired
    private ProcedenciaController procedenciaController;

    @Autowired
    private UtilController utilController;

    @Autowired
    private SearchUI<DistritoWrapper> listaDistrito;

    private ProcedenciaWrapper procedenciaWrapper;

    private boolean saved;

    public ProcedenciaUI() {
        super((java.awt.Frame) null, true);
        initComponents();
        this.setLocationRelativeTo(null);
        this.atajos();
        this.config();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelUI = new com.izune.isabel.ui.PanelUI();
        txtDistritoId = new com.izune.isabel.ui.TextFieldUI();
        jLabel1 = new javax.swing.JLabel();
        txtDistrito = new com.izune.isabel.ui.TextFieldUI();
        jLabel2 = new javax.swing.JLabel();
        cbxTipo = new com.izune.isabel.ui.ComboBoxUI<>();
        txtNombre = new com.izune.isabel.ui.TextFieldUI();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtDireccion = new com.izune.isabel.ui.TextFieldUI();
        jLabel5 = new javax.swing.JLabel();
        txtTelefono = new com.izune.isabel.ui.TextFieldUI();
        btnGrabar = new com.izune.isabel.ui.ButtonUI();
        btnCerrar = new com.izune.isabel.ui.ButtonUI();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtDistritoId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDistritoIdFocusLost(evt);
            }
        });
        txtDistritoId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDistritoIdKeyPressed(evt);
            }
        });

        jLabel1.setText("Distrito");

        txtDistrito.setEditable(false);
        txtDistrito.setEnabled(false);

        jLabel2.setText("Tipo");

        jLabel3.setText("Nombre");

        jLabel4.setText("Dirección");

        jLabel5.setText("Teléfono");

        btnGrabar.setText("Grabar");
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelUILayout = new javax.swing.GroupLayout(panelUI);
        panelUI.setLayout(panelUILayout);
        panelUILayout.setHorizontalGroup(
            panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUILayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelUILayout.createSequentialGroup()
                        .addComponent(btnGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelUILayout.createSequentialGroup()
                        .addComponent(txtDistritoId, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cbxTipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        panelUILayout.setVerticalGroup(
            panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUILayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDistritoId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelUI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelUI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGrabarActionPerformed
        if (panelUI.isVerify()) {
            if (this.isVerify()) {
                try {
                    procedenciaWrapper.setNombre(txtNombre.getText());
                    procedenciaWrapper.setDireccion(txtDireccion.getText());
                    procedenciaWrapper.setTelefono(txtTelefono.getText());
                    procedenciaWrapper.setDistritoId(txtDistritoId.getValueInt());
                    procedenciaWrapper.setTipoProcenciaId(cbxTipo.getValue().getId());
                    procedenciaController.grabar(procedenciaWrapper);
                    this.saved = true;
                    this.dispose();
                } catch (HttpClientErrorException httpError) {
                    this.panelUI.setErrorMessage(httpError);
                } catch (InvalidTextFormat e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }//GEN-LAST:event_btnGrabarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void txtDistritoIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDistritoIdKeyPressed
        if (KeyEvent.VK_ENTER == evt.getKeyCode()) {
            if (this.txtDistritoId.getText().isEmpty() || this.txtDistritoId.getText().equals("0")) {
                try {
                    listaDistrito.setTitle("Buscar Ubigeo");
                    listaDistrito.setSize(900, 500);
                    listaDistrito.setClazz(DistritoWrapper.class);
                    listaDistrito.setData(utilController.listaDistritos());
                    listaDistrito.setVisible(true);
                    if (null != listaDistrito.getObject()) {
                        this.txtDistritoId.setValueInt(listaDistrito.getObject().getId());
                    }
                } catch (HttpClientErrorException httError) {
                    Message.danger(httError);
                } catch (InvalidTextFormat | InvalidTypeColumn | NotFoundColumnAnnotation | IllegalAccessException | IllegalArgumentException ex) {

                }
            }
        }
    }//GEN-LAST:event_txtDistritoIdKeyPressed

    private void txtDistritoIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDistritoIdFocusLost
        if (!this.txtDistritoId.getText().isEmpty() && !this.txtDistritoId.getText().equals("0")) {
            try {
                DistritoWrapper distritoWrapper = procedenciaController
                        .obtenerDistrito(this.txtDistritoId.getValueLong());
                if (null != distritoWrapper) {
                    this.txtDistrito.setText(String.format("%s/%s/%s", distritoWrapper.getDepartamento(),
                            distritoWrapper.getProvincia(), distritoWrapper.getDistrito()));
                }
            } catch (HttpClientErrorException errorHttp) {
                this.panelUI.setErrorMessage(errorHttp);
                this.txtDistritoId.hasError();
                this.txtDistritoId.requestFocus();
            } catch (InvalidTextFormat ex) {

            }
        }
    }//GEN-LAST:event_txtDistritoIdFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.izune.isabel.ui.ButtonUI btnCerrar;
    private com.izune.isabel.ui.ButtonUI btnGrabar;
    private com.izune.isabel.ui.ComboBoxUI<TipoProcedenciaWrapper> cbxTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private com.izune.isabel.ui.PanelUI panelUI;
    private com.izune.isabel.ui.TextFieldUI txtDireccion;
    private com.izune.isabel.ui.TextFieldUI txtDistrito;
    private com.izune.isabel.ui.TextFieldUI txtDistritoId;
    private com.izune.isabel.ui.TextFieldUI txtNombre;
    private com.izune.isabel.ui.TextFieldUI txtTelefono;
    // End of variables declaration//GEN-END:variables

    private void config() {
        this.txtDistritoId.setRequired(true);
        this.txtNombre.setRequired(true);
        this.txtDireccion.setRequired(true);
        this.txtDistritoId.setFormat(Isabel.NUMBER, true);
        this.cbxTipo.setClazz(TipoProcedenciaWrapper.class);
    }

    public void cargarDatos() {
        try {
            txtNombre.setText(procedenciaWrapper.getNombre());
            txtDireccion.setText(procedenciaWrapper.getDireccion());
            txtTelefono.setText(procedenciaWrapper.getTelefono());
            cbxTipo.setValue(procedenciaWrapper.getTipoProcenciaId());
            txtDistritoId.setValueInt(procedenciaWrapper.getDistritoId());
            txtDistritoIdFocusLost(null);
        } catch (InvalidTextFormat | IllegalAccessException | IllegalArgumentException | NoSuchFieldException e) {
            System.out.println(e.getMessage());
        }

    }

    private void atajos() {
        this.rootPane.registerKeyboardAction(e -> {
            btnGrabar.doClick();
        }, KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW);

        this.rootPane.registerKeyboardAction(e -> {

            btnCerrar.doClick();
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

    }

    private void cargarCombos() {
        try {
            this.cbxTipo.setItems(procedenciaController.listaTipoProcedencias());
        } catch (NotFoundDisplayValueAnnotation | IllegalAccessException | IllegalArgumentException | HttpClientErrorException e) {
        }
    }

    @Override
    public void setObject(ProcedenciaWrapper object) {
        this.procedenciaWrapper = object;
    }

    @Override
    public ProcedenciaWrapper getObject() {
        return this.procedenciaWrapper;
    }

    @Override
    public void setEdit(boolean edit) {
        this.panelUI.reset();
        this.saved = false;
        this.cargarCombos();
        if (edit) {
            this.cargarDatos();
            this.setTitle("Editar procedencia");
        } else {
            this.procedenciaWrapper = new ProcedenciaWrapper();
            procedenciaWrapper.setEstado(1);
            this.setTitle("Nueva procedencia");

        }
    }

    @Override
    public boolean isVerify() {
        return true;
    }

    @Override
    public boolean isSaved() {
        return this.saved;
    }

}
