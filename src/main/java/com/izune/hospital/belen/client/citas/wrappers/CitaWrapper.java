/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

import com.izune.isabel.Isabel;
import com.izune.isabel.annotation.table.Column;
import com.izune.isabel.annotation.table.Format;
import java.util.Calendar;

/**
 *
 * @author aneyra
 */
public class CitaWrapper {

    @Column(name = "Codigo", width = 40)
    private long id;

    @Column(name = "Fecha", width = 80)
    @Format(constant = Isabel.DATE, pattern = "dd/MM/yyyy")
    private Calendar fecha;

    @Column(name = "Nro historia cĺínica", width = 80)
    private String numeroHistoria;

    @Column(name = "Paciente", width = 200)
    private String paciente;

    @Column(name = "Especialidad", width = 200)
    private String especialidad;

    @Column(name = "Medico", width = 200)
    private String medico;

    @Column(visible = false)
    private int especialidadId;

    @Column(visible = false)
    private int tipoCitaId;

    @Column(visible = false)
    private String referencia;

    @Column(visible = false)
    private long pacienteId;

    @Column(visible = false)
    private long cronogramaId;

    @Column(visible = false)
    private long referenciaId;

    @Column(visible = false)
    private int procedenciaId;

    @Column(visible = false)
    private Calendar fechaReferencia;

    @Column(visible = false)
    private long medicoId;

    @Column(visible = false)
    private int tipoPacienteId;

    @Column(visible = false)
    private String procedencia;

    @Column(visible = false)
    private String numeroDocumento;

    public CitaWrapper() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public String getNumeroHistoria() {
        return numeroHistoria;
    }

    public void setNumeroHistoria(String numeroHistoria) {
        this.numeroHistoria = numeroHistoria;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public int getEspecialidadId() {
        return especialidadId;
    }

    public void setEspecialidadId(int especialidadId) {
        this.especialidadId = especialidadId;
    }

    public int getTipoCitaId() {
        return tipoCitaId;
    }

    public void setTipoCitaId(int tipoCitaId) {
        this.tipoCitaId = tipoCitaId;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public long getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(long pacienteId) {
        this.pacienteId = pacienteId;
    }

    public long getCronogramaId() {
        return cronogramaId;
    }

    public void setCronogramaId(long cronogramaId) {
        this.cronogramaId = cronogramaId;
    }

    public long getReferenciaId() {
        return referenciaId;
    }

    public void setReferenciaId(long referenciaId) {
        this.referenciaId = referenciaId;
    }

    public int getProcedenciaId() {
        return procedenciaId;
    }

    public void setProcedenciaId(int procedenciaId) {
        this.procedenciaId = procedenciaId;
    }

    public Calendar getFechaReferencia() {
        return fechaReferencia;
    }

    public void setFechaReferencia(Calendar fechaReferencia) {
        this.fechaReferencia = fechaReferencia;
    }

    public long getMedicoId() {
        return medicoId;
    }

    public void setMedicoId(long medicoId) {
        this.medicoId = medicoId;
    }

    public int getTipoPacienteId() {
        return tipoPacienteId;
    }

    public void setTipoPacienteId(int tipoPacienteId) {
        this.tipoPacienteId = tipoPacienteId;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

}
