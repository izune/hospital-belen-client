package com.izune.hospital.belen.client.citas.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

import com.izune.hospital.belen.client.citas.wrappers.EspecialidadWrapper;
import com.izune.hospital.belen.client.config.Resource;
import com.izune.isabel.net.RestClient;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class EspecialidadController {

    @Autowired
    private RestClient<EspecialidadWrapper[]> listEspecialidad;

    @Autowired
    private RestClient<EspecialidadWrapper> especialidad;

    @Autowired
    private RestClient<Object> deleteClient;

    @Autowired
    private RestClient<Boolean> search;

    public List<EspecialidadWrapper> listaEspecialidades() throws HttpClientErrorException {

        List<EspecialidadWrapper> especialidades = Arrays
                .asList(listEspecialidad.resource("/citas/especialidad")
                        .authorization(Resource.TOKEN).get().clazz(EspecialidadWrapper[].class).build());
        return especialidades;

    }

    public EspecialidadWrapper grabar(EspecialidadWrapper especialidadWrapper) throws HttpClientErrorException {

        especialidad.resource("/citas/especialidad")
                .authorization(Resource.TOKEN).body(especialidadWrapper).clazz(EspecialidadWrapper.class);
        if (0 == especialidadWrapper.getId()) {
            especialidadWrapper = especialidad.post().build();
        } else {
            especialidadWrapper = especialidad.put().build();
        }

        return especialidadWrapper;
    }

    public EspecialidadWrapper obtenerEspecialidad(long id) throws HttpClientErrorException {

        EspecialidadWrapper especialidadWrapper = especialidad.resource("/citas/especialidad")
                .resourceId(id).authorization(Resource.TOKEN).get().clazz(EspecialidadWrapper.class).build();

        return especialidadWrapper;

    }

    public boolean eliminar(int id) throws HttpClientErrorException {

        deleteClient.resource("/citas/especialidad").resourceId(id)
                .authorization(Resource.TOKEN).clazz(Object.class).delete().build();
        return true;

    }

    public boolean existe(String nombre, int excluir) throws HttpClientErrorException {

        return search.resource("/citas/especialidad/search")
                .authorization(Resource.TOKEN)
                .param("nombre", nombre)
                .param("excluir", excluir)
                .clazz(Boolean.class)
                .get().build();

    }
}
