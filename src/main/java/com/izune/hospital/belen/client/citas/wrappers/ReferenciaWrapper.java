/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

import java.util.Calendar;

/**
 *
 * @author aneyra
 */
public class ReferenciaWrapper {

    private long id;
    private String numero;
    private long pacienteId;
    private String paciente;
    private Calendar fechaReferencia;
    private int estado;
    private int procedenciaId;
    private String procedencia;
    private boolean status;
    private String numeroHistoria;
    private String numeroDocumento;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(long pacienteId) {
        this.pacienteId = pacienteId;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public Calendar getFechaReferencia() {
        return fechaReferencia;
    }

    public void setFechaReferencia(Calendar fechaReferencia) {
        this.fechaReferencia = fechaReferencia;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getProcedenciaId() {
        return procedenciaId;
    }

    public void setProcedenciaId(int procedenciaId) {
        this.procedenciaId = procedenciaId;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getNumeroHistoria() {
        return numeroHistoria;
    }

    public void setNumeroHistoria(String numeroHistoria) {
        this.numeroHistoria = numeroHistoria;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    
}
