/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.controllers;

import com.izune.hospital.belen.client.citas.wrappers.CancelaCitaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.CitaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.CitasPorEspecialidadWrapper;
import com.izune.hospital.belen.client.citas.wrappers.ReferenciaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.TipoCitaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.TipoPacienteWrapper;
import com.izune.hospital.belen.client.config.Resource;
import com.izune.isabel.net.RestClient;
import com.izune.isabel.utils.UtilArrays;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author aneyra
 */
@Controller
public class CitaController {

    @Autowired
    private RestClient<TipoPacienteWrapper[]> listTipoPaciente;

    @Autowired
    private RestClient<TipoCitaWrapper[]> listTipoCita;

    @Autowired
    private RestClient<Object> citaRest;

    @Autowired
    private RestClient<Object[]> listCita;

    @Autowired
    private RestClient<CitasPorEspecialidadWrapper[]> listCitaPorEspecialidad;

    @Autowired
    private RestClient<ReferenciaWrapper> referencia;

    public List<TipoPacienteWrapper> obtenerTiposPaciente() throws HttpClientErrorException {

        return Arrays.asList(listTipoPaciente.resource("/citas/tipo-paciente").authorization(Resource.TOKEN)
                .clazz(TipoPacienteWrapper[].class).get().build());

    }

    public boolean grabarCita(CitaWrapper citaWrapper) {

        citaRest.resource("/citas")
                .authorization(Resource.TOKEN)
                .post().body(citaWrapper).build();
        return true;

    }

    public List<CitaWrapper> listarCitas(String desde, String hasta) throws IllegalAccessException, InstantiationException, ParseException {

        return UtilArrays.asList(listCita.resource("/citas/search")
                .param("desde", desde)
                .param("hasta", hasta)
                .authorization(Resource.TOKEN).clazz(Object[].class)
                .get().build(), CitaWrapper.class);

    }

    public List<CitaWrapper> listarCitas(String desde, String hasta, int tipoPaciente) throws IllegalAccessException, InstantiationException, ParseException {

        return UtilArrays.asList(listCita.resource("/citas/search")
                .param("desde", desde)
                .param("hasta", hasta)
                .param("tipoPaciente", tipoPaciente)
                .authorization(Resource.TOKEN).clazz(Object[].class)
                .get().build(), CitaWrapper.class);

    }

    public List<TipoCitaWrapper> obtenerTiposCita() throws HttpClientErrorException {

        return Arrays.asList(listTipoCita.resource("/citas/tipo-cita").authorization(Resource.TOKEN)
                .clazz(TipoCitaWrapper[].class).get().build());

    }

    public ReferenciaWrapper obtenerRefrencia(String numero) throws HttpClientErrorException {

        return referencia
                .resource("/citas/referencia/search")
                .param("numero", numero)
                .authorization(Resource.TOKEN)
                .clazz(ReferenciaWrapper.class)
                .get()
                .build();
    }

    public void cancelarCita(CancelaCitaWrapper cancelaCitaWrapper) throws HttpClientErrorException {

        citaRest.resource("/citas/cancelar")
                .body(cancelaCitaWrapper)
                .authorization(Resource.TOKEN)
                .delete()
                .build();
    }

    public List<CitasPorEspecialidadWrapper> obtenerCitasPorEspecialidad(Long persona, int especialidad, String desde, String hasta) throws HttpClientErrorException {

        return Arrays.asList(listCitaPorEspecialidad.resource("/citas/reporte/especialidad/search")
                .param("persona", persona)
                .param("especialidad", especialidad)
                .param("desde", desde)
                .param("hasta", hasta)
                .authorization(Resource.TOKEN)
                .clazz(CitasPorEspecialidadWrapper[].class)
                .get()
                .build()
        );

    }
}
