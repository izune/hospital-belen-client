package com.izune.hospital.belen.client.citas.controllers;

import com.izune.hospital.belen.client.citas.wrappers.DistritoWrapper;
import com.izune.hospital.belen.client.citas.wrappers.ProcedenciaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.TipoProcedenciaWrapper;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

import com.izune.hospital.belen.client.config.Resource;
import com.izune.isabel.net.RestClient;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class ProcedenciaController {

    @Autowired
    private RestClient<ProcedenciaWrapper[]> listProcedencia;

    @Autowired
    private RestClient<TipoProcedenciaWrapper[]> listTipoProcedenciaRest;

    @Autowired
    private RestClient<Object> procedencia;

    @Autowired
    private RestClient<DistritoWrapper> distritoRest;

    @Autowired
    private RestClient<ProcedenciaWrapper> restClient;

    public List<ProcedenciaWrapper> listaProcedencia() throws HttpClientErrorException {

        List<ProcedenciaWrapper> procedencia = Arrays
                .asList(listProcedencia.resource("/citas/procedencia")
                        .authorization(Resource.TOKEN)
                        .clazz(ProcedenciaWrapper[].class)
                        .get().build());

        return procedencia;

    }

    public boolean grabar(ProcedenciaWrapper procedenciaWrapper) throws HttpClientErrorException {

        procedencia.resource("/citas/procedencia")
                .authorization(Resource.TOKEN)
                .body(procedenciaWrapper);

        if (0 == procedenciaWrapper.getId()) {
            procedencia.post().build();
        } else {
            procedencia.put().build();
        }

        return true;
    }

    public ProcedenciaWrapper obtenerProcedencia(long id) throws HttpClientErrorException {

        ProcedenciaWrapper procedencia
                = restClient.resource("/citas/procedencia")
                        .resourceId(id)
                        .authorization(Resource.TOKEN)
                        .clazz(ProcedenciaWrapper.class)
                        .get().build();
        return procedencia;

    }

    public boolean anular(int id) throws HttpClientErrorException {

        procedencia.resource("/citas/procedencia").resourceId(id)
                .authorization(Resource.TOKEN)
                .delete().build();
        return true;

    }

    public List<TipoProcedenciaWrapper> listaTipoProcedencias() throws HttpClientErrorException {
        List<TipoProcedenciaWrapper> tipoProcedencias = new ArrayList<>();
        tipoProcedencias = Arrays.asList(listTipoProcedenciaRest
                .resource("/citas/tipo-procedencia")
                .authorization(Resource.TOKEN)
                .clazz(TipoProcedenciaWrapper[].class)
                .get().build());
        return tipoProcedencias;
    }

    public DistritoWrapper obtenerDistrito(long id) throws HttpClientErrorException {

        return distritoRest.authorization(Resource.TOKEN)
                .resource("/utils/distrito")
                .resourceId(id)
                .clazz(DistritoWrapper.class)
                .get().build();

    }

}
