/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author aneyra
 */
public class CronogramaWrapper {

    private long id;
    private Calendar fecha;
    private List<CronogramaDetalleWrapper> detalle;
    private List<CronogramaDetalleWrapper> eliminados;

    public CronogramaWrapper() {
        this.detalle = new ArrayList<>();
        this.eliminados = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public List<CronogramaDetalleWrapper> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<CronogramaDetalleWrapper> detalle) {
        this.detalle = detalle;
    }

    public List<CronogramaDetalleWrapper> getEliminados() {
        return eliminados;
    }

    public void setEliminados(List<CronogramaDetalleWrapper> eliminados) {
        this.eliminados = eliminados;
    }

}
