package com.izune.hospital.belen.client.personas.wrappers;

import com.izune.isabel.annotation.combobox.DisplayValue;
import com.izune.isabel.annotation.combobox.IdValue;

/**
 *
 * @author aneyra
 */
public class DocumentoIdentidadWrapper {

    @IdValue
    private int id;
    @DisplayValue
    private String nombre;
    private int longitud;

    public DocumentoIdentidadWrapper() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

}
