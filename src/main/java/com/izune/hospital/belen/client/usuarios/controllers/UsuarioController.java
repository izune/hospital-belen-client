package com.izune.hospital.belen.client.usuarios.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.izune.hospital.belen.client.config.Resource;
import java.util.List;

import org.springframework.stereotype.Controller;
import com.izune.hospital.belen.client.usuarios.wrappers.UsuarioWrapper;
import com.izune.isabel.net.RestClient;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;

@Controller
public class UsuarioController {

    @Autowired
    private RestClient<UsuarioWrapper[]> listUsers;

    @Autowired
    private RestClient<Object> restClient;

    @Autowired
    private RestClient<UsuarioWrapper> userRest;

    public List<UsuarioWrapper> listaUsuarios() throws HttpClientErrorException {

        return Arrays.asList(listUsers.resource("/users")
                .authorization(Resource.TOKEN)
                .clazz(UsuarioWrapper[].class)                
                .get()
                .build()
        );
    }

    public boolean grabarUsuario(UsuarioWrapper usuarioWrapper) throws HttpClientErrorException {

        UsuarioWrapper msj = userRest.resource("/users")
                .authorization(Resource.TOKEN)
                .clazz(UsuarioWrapper.class)
                .body(usuarioWrapper)
                .put()
                .build();
        System.out.println(msj.getClave());
        return true;

    }
}
