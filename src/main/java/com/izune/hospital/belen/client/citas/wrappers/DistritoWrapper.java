/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

import com.izune.isabel.annotation.table.Column;

public class DistritoWrapper {

    @Column(name = "Codigo")
    private int id;
    @Column(name = "Distrito")
    private String distrito;
    @Column(name = "Provincia")
    private String provincia;
    @Column(name = "Departamento")
    private String departamento;

    public DistritoWrapper() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

}
