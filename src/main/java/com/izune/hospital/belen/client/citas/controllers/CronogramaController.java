package com.izune.hospital.belen.client.citas.controllers;

import com.izune.hospital.belen.client.citas.wrappers.CronogramaDetalleWrapper;
import com.izune.hospital.belen.client.citas.wrappers.CronogramaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.DisponibilidadWrapper;
import com.izune.hospital.belen.client.config.Resource;
import com.izune.isabel.net.RestClient;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

@Controller
public class CronogramaController {

    @Autowired
    private RestClient<CronogramaDetalleWrapper[]> listCronograma;

    @Autowired
    private RestClient<DisponibilidadWrapper[]> listHorario;

    @Autowired
    private RestClient<Object> restSave;

    @Autowired
    private RestClient<Boolean> existe;

    @Autowired
    private RestClient<Integer> cantidad;

    public List<CronogramaDetalleWrapper> obtenerCronograma(String fecha) throws HttpClientErrorException {

        return Arrays.asList(listCronograma.resource("/citas/cronograma-detalle/search").authorization(Resource.TOKEN)
                .param("fecha", fecha).clazz(CronogramaDetalleWrapper[].class)
                .get().build());

    }

    public boolean grabar(CronogramaWrapper cronogramaWrapper) throws HttpClientErrorException {

        restSave.resource("/citas/cronograma").authorization(Resource.TOKEN)
                .body(cronogramaWrapper).post().build();

        return true;
    }

    public boolean existeCronogramaCita(Long id) throws HttpClientErrorException {

        return existe.resource("/citas/cronograma-detalle/exists/search")
                .param("cronogramaId", id)
                .authorization(Resource.TOKEN)
                .clazz(Boolean.class)
                .get().build();

    }

    public Integer cantidadCitas(Long id) throws HttpClientErrorException {

        return cantidad.resource("/citas/numero/search")
                .param("cronogramaId", id)
                .authorization(Resource.TOKEN)
                .clazz(Integer.class)
                .get().build();

    }

    public List<DisponibilidadWrapper> obtenerHorario(int especialidadId) throws HttpClientErrorException {

        return Arrays.asList(listHorario
                .resource("/citas/horario")
                .resourceId(especialidadId)
                .authorization(Resource.TOKEN)
                .clazz(DisponibilidadWrapper[].class)
                .get().build());

    }
}
