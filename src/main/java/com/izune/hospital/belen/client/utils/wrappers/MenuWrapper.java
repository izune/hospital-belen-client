package com.izune.hospital.belen.client.utils.wrappers;

public class MenuWrapper {
	private int menuId;
	private String nombre;
	private String ui;
	private int menu;

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUi() {
		return ui;
	}

	public void setUi(String ui) {
		this.ui = ui;
	}

	public int getMenu() {
		return menu;
	}

	public void setMenu(int menu) {
		this.menu = menu;
	}

	@Override
	public String toString() {

		return this.nombre;
	}

}
