/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.personas.controllers;

import com.izune.hospital.belen.client.config.Resource;
import com.izune.hospital.belen.client.personas.wrappers.DocumentoIdentidadWrapper;
import com.izune.hospital.belen.client.personas.wrappers.HistoriaClinicaWapper;
import com.izune.hospital.belen.client.personas.wrappers.PacienteWrapper;
import com.izune.isabel.net.RestClient;
import com.izune.isabel.utils.UtilArrays;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author aneyra
 */
@Controller
public class PacienteController {

    @Autowired
    private RestClient<Object[]> listPaciente;

    @Autowired
    private RestClient<PacienteWrapper> paciente;

    @Autowired
    private RestClient<DocumentoIdentidadWrapper[]> listTipoDocumento;

    @Autowired
    private RestClient<Boolean> existe;

    @Autowired
    private RestClient<Object> rest;

    public List<PacienteWrapper> listarPacientesConHistoria() throws HttpClientErrorException, IllegalAccessException, InstantiationException, ParseException {

        return UtilArrays.asList(listPaciente
                .authorization(Resource.TOKEN)
                .resource("/personas/paciente/con-historia")
                .clazz(Object[].class)
                .get().build(), PacienteWrapper.class);

    }

    public List<PacienteWrapper> listarPacientesSinHistoria() throws HttpClientErrorException, IllegalAccessException, InstantiationException, ParseException {

        return UtilArrays.asList(listPaciente
                .authorization(Resource.TOKEN)
                .resource("/personas/paciente/sin-historia")
                .clazz(Object[].class)
                .get().build(), PacienteWrapper.class);

    }

    public List<PacienteWrapper> listarPacientes() throws HttpClientErrorException, IllegalAccessException, InstantiationException, ParseException {

        return UtilArrays.asList(listPaciente
                .authorization(Resource.TOKEN)
                .resource("/personas/paciente")
                .clazz(Object[].class)
                .get().build(), PacienteWrapper.class);

    }

    public List<DocumentoIdentidadWrapper> listarDocumentoIdentidad() throws HttpClientErrorException {

        return Arrays.asList(listTipoDocumento
                .authorization(Resource.TOKEN)
                .resource("/personas/documento-identidad")
                .clazz(DocumentoIdentidadWrapper[].class)
                .get().build());

    }

    public boolean grabar(PacienteWrapper pacienteWrapper) throws HttpClientErrorException {

        rest.resource("/personas/paciente")
                .authorization(Resource.TOKEN)
                .body(pacienteWrapper);

        if (0 == pacienteWrapper.getId()) {
            rest.post().build();
        } else {
            rest.put().build();
        }
        return true;
    }

    public PacienteWrapper obtenerPaciente(long id) throws HttpClientErrorException {

        return paciente.resource("/personas/paciente")
                .resourceId(id)
                .authorization(Resource.TOKEN)
                .clazz(PacienteWrapper.class)
                .get().build();

    }

    public Boolean existeNumeroHistoria(String numeroHistoria, long excluir) throws HttpClientErrorException {

        return existe.resource("/personas/paciente/search")
                .authorization(Resource.TOKEN)
                .param("numeroHistoria", numeroHistoria)
                .param("excluir", excluir)
                .clazz(Boolean.class).get().build();

    }

    public void grabarHistoriaClinica(HistoriaClinicaWapper historiaClinicaWapper) throws HttpClientErrorException {

        rest.resource("/personas/historia-clinica")
                .authorization(Resource.TOKEN)
                .body(historiaClinicaWapper);

        if (0 == historiaClinicaWapper.getId()) {
            rest.post();
        } else {
            rest.put();
        }

        rest.build();
    }

}
