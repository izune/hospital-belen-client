package com.izune.hospital.belen.client.config;

public class Resource {

    public static String TOKEN;
    public static final String PROPERTIES = "META-INF/application.properties";
    public static final String ASPECTS = "com.izune.hospital.belen.client.config.aspect";

    public static final String USUARIOS_VIEWS = "com.izune.hospital.belen.client.usuarios.views";
    public static final String USUARIOS_CONTROLLERS = "com.izune.hospital.belen.client.usuarios.controllers";
    public static final String USUARIOS_WRAPPERS = "com.izune.hospital.belen.client.usuarios.wrappers";

    public static final String UTILS_VIEWS = "com.izune.hospital.belen.client.utils.views";
    public static final String UTILS_CONTROLLERS = "com.izune.hospital.belen.client.utils.controllers";
    public static final String UTILS_WRAPPERS = "com.izune.hospital.belen.client.utils.wrappers";

    public static final String CITAS_VIEWS = "com.izune.hospital.belen.client.citas.views";
    public static final String CITAS_CONTROLLERS = "com.izune.hospital.belen.client.citas.controllers";
    public static final String CITAS_WRAPPERS = "com.izune.hospital.belen.client.citas.wrappers";

    public static final String PERSONAS_VIEWS = "com.izune.hospital.belen.client.personas.views";
    public static final String PERSONAS_CONTROLLERS = "com.izune.hospital.belen.client.personas.controllers";
    public static final String PERSONAS_WRAPPERS = "com.izune.hospital.belen.client.personas.wrappers";

}
