package com.izune.hospital.belen.client.citas.wrappers;

import com.izune.isabel.annotation.table.Column;
import javax.swing.SwingConstants;

public class EspecialidadWrapper {

    @Column(name = "Codigo", width = 80, align = SwingConstants.RIGHT)
    private int id;
    @Column(name = "Descripcion", width = 500)
    private String nombre;
    @Column(visible = false)
    private int estado;

    public EspecialidadWrapper() {
        // TODO Auto-generated constructor stub
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

}
