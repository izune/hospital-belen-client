/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

import com.izune.isabel.Isabel;
import com.izune.isabel.annotation.table.Column;
import com.izune.isabel.annotation.table.Format;
import java.util.Calendar;
import javax.swing.SwingConstants;

/**
 *
 * @author aneyra
 */
public class DisponibilidadWrapper {

    @Column(name = "Codigo", align = SwingConstants.RIGHT)
    private long id;

    @Column(name = "Fecha")
    @Format(constant = Isabel.DATE, pattern = "dd-MM-yyyy")
    private Calendar fecha;

    @Column(visible = false)
    private long medicoId;

    @Column(name = "Medico")
    private String medico;
    
    @Column(name = "Citas disponibles", align = SwingConstants.RIGHT)
    @Format(constant = Isabel.NUMBER)
    private int numeroCitas;

    public DisponibilidadWrapper() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public long getMedicoId() {
        return medicoId;
    }

    public void setMedicoId(long medicoId) {
        this.medicoId = medicoId;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public int getNumeroCitas() {
        return numeroCitas;
    }

    public void setNumeroCitas(int numeroCitas) {
        this.numeroCitas = numeroCitas;
    }

}
