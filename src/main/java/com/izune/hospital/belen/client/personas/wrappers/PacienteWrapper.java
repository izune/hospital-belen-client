package com.izune.hospital.belen.client.personas.wrappers;

import com.izune.isabel.annotation.table.Column;
import java.util.Calendar;

/**
 *
 * @author aneyra
 */
public class PacienteWrapper {

    @Column(name = "Codigo", width = 50)
    private long id;

    @Column(name = "Nro Historia Clínica", width = 100)
    private String numeroHistoria;

    @Column(name = "Apellido Paterno", width = 100)
    private String apellidoPaterno;

    @Column(name = "Apellido Materno", width = 100)
    private String apellidoMaterno;

    @Column(name = "Nombres", width = 200)
    private String nombre;

    @Column(name = "Dcoumento Identidad", width = 80)
    private String numeroDocumento;

    @Column(name = "Dirección", width = 300)
    private String direccion;

    @Column(name = "Telefono", width = 80)
    private String telefono;

    @Column(visible = false)
    private Calendar fechaNacimiento;

    @Column(visible = false)
    private int documentoIdentidadId;

    @Column(visible = false)
    private long historiaClinicaId;
    
    @Column(visible = false)
    private int distritoId;

    @Column(visible = false)
    private int estado;

    public PacienteWrapper() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNumeroHistoria() {
        return numeroHistoria;
    }

    public void setNumeroHistoria(String numeroHistoria) {
        this.numeroHistoria = numeroHistoria;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Calendar getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Calendar fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getDocumentoIdentidadId() {
        return documentoIdentidadId;
    }

    public void setDocumentoIdentidadId(int documentoIdentidadId) {
        this.documentoIdentidadId = documentoIdentidadId;
    }

    public int getDistritoId() {
        return distritoId;
    }

    public void setDistritoId(int distritoId) {
        this.distritoId = distritoId;
    }

    public long getHistoriaClinicaId() {
        return historiaClinicaId;
    }

    public void setHistoriaClinicaId(long historiaClinicaId) {
        this.historiaClinicaId = historiaClinicaId;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

}
