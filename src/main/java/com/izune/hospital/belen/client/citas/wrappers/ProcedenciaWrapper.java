package com.izune.hospital.belen.client.citas.wrappers;

import com.izune.isabel.annotation.table.Column;
import javax.swing.SwingConstants;

public class ProcedenciaWrapper {

    @Column(name = "Código", width = 10, align = SwingConstants.RIGHT)
    private int id;
    @Column(visible = false)
    private int distritoId;
    @Column(name = "Tipo deProcedencia", width = 80)
    private String procedencia;
    @Column(name = "Descripción", width = 200)
    private String nombre;
    @Column(name = "Dirección", width = 200)
    private String direccion;
    @Column(name = "Teléfono", width = 50)
    private String telefono;
    @Column(visible = false)
    private int tipoProcenciaId;
    @Column(visible = false)
    private int estado;
    @Column(visible = false)
    private String distrito;

    public ProcedenciaWrapper() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getDistritoId() {
        return distritoId;
    }

    public void setDistritoId(int distritoId) {
        this.distritoId = distritoId;
    }

    public int getTipoProcenciaId() {
        return tipoProcenciaId;
    }

    public void setTipoProcenciaId(int tipoProcenciaId) {
        this.tipoProcenciaId = tipoProcenciaId;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

}
