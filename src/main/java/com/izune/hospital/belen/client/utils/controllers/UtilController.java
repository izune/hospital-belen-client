/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.utils.controllers;

import com.izune.hospital.belen.client.citas.wrappers.DistritoWrapper;
import com.izune.hospital.belen.client.config.Resource;
import com.izune.isabel.net.RestClient;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author aneyra
 */
@Controller
public class UtilController {

    @Autowired
    private RestClient<DistritoWrapper[]> listDistritos;

    @Autowired
    private RestClient<Calendar> time;

    public List<DistritoWrapper> listaDistritos() throws HttpClientErrorException {

        return Arrays
                .asList(listDistritos.resource("/utils/distrito")
                        .authorization(Resource.TOKEN)
                        .clazz(DistritoWrapper[].class)
                        .get().build());

    }

    public Calendar getTime() {
        return time
                .resource("/utils/time")
                .authorization(Resource.TOKEN)
                .clazz(Calendar.class)
                .get()
                .build();
    }
}
