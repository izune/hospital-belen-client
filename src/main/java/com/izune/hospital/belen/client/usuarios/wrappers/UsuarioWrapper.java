package com.izune.hospital.belen.client.usuarios.wrappers;

import com.izune.isabel.annotation.table.Column;
import javax.swing.SwingConstants;

public class UsuarioWrapper {

    @Column(name = "Codigo", width = 10)
    private int id;

    @Column(name = "Nombre", width = 200)
    private String nombre;

    @Column(name = "Clave", width = 200)
    private String clave;

    @Column(name = "Estado", width = 50, align = SwingConstants.CENTER)
    private boolean active;

    public UsuarioWrapper() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

}
