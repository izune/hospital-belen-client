/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

/**
 *
 * @author aneyra
 */
public class CancelaCitaWrapper {

    private Long id;
    private String motivo;

    public CancelaCitaWrapper() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

}
