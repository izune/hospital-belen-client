package com.izune.hospital.belen.client.usuarios.controllers;

import com.izune.hospital.belen.client.config.Resource;
import com.izune.hospital.belen.client.usuarios.wrappers.TokenWrapper;
import com.izune.isabel.net.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class LoginController {

    @Autowired
    public RestClient<TokenWrapper> restcliet;

    public boolean login(String usuario, String clave) {

        try {
            TokenWrapper token = restcliet.resource("/tokens").basicAuth(usuario, clave).post().clazz(TokenWrapper.class).build();

            if (!token.getToken().isEmpty()) {
                Resource.TOKEN = token.getToken();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
