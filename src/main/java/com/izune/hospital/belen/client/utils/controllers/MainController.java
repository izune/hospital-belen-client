package com.izune.hospital.belen.client.utils.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;


import com.izune.hospital.belen.client.config.Resource;
import com.izune.hospital.belen.client.utils.wrappers.MenuWrapper;
import com.izune.isabel.net.RestClient;
import org.springframework.beans.factory.annotation.Autowired;


@Controller
public class MainController {
    
        @Autowired
        private RestClient<MenuWrapper[]> menuRest;

	public List<MenuWrapper> getMenu() {
		try {
			List<MenuWrapper> menus = Arrays.asList(menuRest
					.resource("/utils/menu").authorization(Resource.TOKEN).get().clazz(MenuWrapper[].class).build());
			
                        return menus;
		} catch (HttpClientErrorException httpError) {
			System.out.println(httpError.getMessage());
		}

		return null;
	}
}
