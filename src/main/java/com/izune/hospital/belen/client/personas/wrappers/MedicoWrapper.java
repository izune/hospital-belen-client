package com.izune.hospital.belen.client.personas.wrappers;

import com.izune.isabel.annotation.table.Column;

/**
 *
 * @author aneyra
 */
public class MedicoWrapper {

    @Column(name = "Codigo", width = 50)
    private long id;

    @Column(name = "Apellido Paterno", width = 200)
    private String apellidoPaterno;

    @Column(name = "Apellido Materno", width = 200)
    private String apellidoMaterno;

    @Column(name = "Nombres", width = 300)
    private String nombre;

    @Column(name = "Especialidad", width = 200)
    private String especialidad;

    @Column(visible = false)
    private int especialidadId;

    @Column(visible = false)
    private int documentoIdentidadId;

    @Column(visible = false)
    private String telefono;

    @Column(visible = false)
    private String numeroDocumento;

    @Column(visible = false)
    private String cmp;

    @Column(visible = false)
    private int estado;

    public MedicoWrapper() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public int getEspecialidadId() {
        return especialidadId;
    }

    public void setEspecialidadId(int especialidadId) {
        this.especialidadId = especialidadId;
    }

    public int getDocumentoIdentidadId() {
        return documentoIdentidadId;
    }

    public void setDocumentoIdentidadId(int documentoIdentidadId) {
        this.documentoIdentidadId = documentoIdentidadId;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getCmp() {
        return cmp;
    }

    public void setCmp(String cmp) {
        this.cmp = cmp;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

}
