package com.izune.hospital.belen.client.config;

import com.izune.isabel.net.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;

@PropertySource(Resource.PROPERTIES)
@Configuration
@ComponentScan(basePackages = {
    Resource.USUARIOS_VIEWS,
    Resource.USUARIOS_CONTROLLERS,
    Resource.USUARIOS_WRAPPERS,
    Resource.UTILS_VIEWS,    
    Resource.UTILS_CONTROLLERS,
    Resource.UTILS_WRAPPERS,
    Resource.CITAS_VIEWS,
    Resource.CITAS_CONTROLLERS,
    Resource.CITAS_WRAPPERS,
    Resource.PERSONAS_VIEWS,
    Resource.PERSONAS_CONTROLLERS,
    Resource.PERSONAS_WRAPPERS

})
public class AppConfig {

    @Autowired
    private Environment environment;

    @Bean
    @Scope("prototype")
    public RestClient<?> restClient() {
        String url = environment.getProperty("server.api.url");

        return new RestClient<>(url);
    }

}
