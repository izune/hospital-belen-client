package com.izune.hospital.belen.client.citas.wrappers;

public class CitasPorEspecialidadWrapper {

    private int anio;
    private String especialidad;
    private Long id;
    private String periodo;
    private String numeroHistoria;
    private String paciente;
    private String fechaCancelada;
    private String estado;

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getNumeroHistoria() {
        return numeroHistoria;
    }

    public void setNumeroHistoria(String numeroHistoria) {
        this.numeroHistoria = numeroHistoria;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getFechaCancelada() {
        return fechaCancelada;
    }

    public void setFechaCancelada(String fechaCancelada) {
        this.fechaCancelada = fechaCancelada;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public CitasPorEspecialidadWrapper() {
    }

}
