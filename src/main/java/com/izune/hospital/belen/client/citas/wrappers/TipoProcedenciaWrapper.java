/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.wrappers;

import com.izune.isabel.annotation.combobox.DisplayValue;
import com.izune.isabel.annotation.combobox.IdValue;

/**
 *
 * @author aneyra
 */
public class TipoProcedenciaWrapper {

    @IdValue
    private int id;
    @DisplayValue
    private String nombre;

    public TipoProcedenciaWrapper() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
