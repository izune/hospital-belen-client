/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.izune.hospital.belen.client.citas.views;

import com.izune.hospital.belen.client.citas.controllers.CitaController;
import com.izune.hospital.belen.client.citas.controllers.CronogramaController;
import com.izune.hospital.belen.client.citas.controllers.EspecialidadController;
import com.izune.hospital.belen.client.citas.controllers.ProcedenciaController;
import com.izune.hospital.belen.client.citas.wrappers.CitaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.DisponibilidadWrapper;
import com.izune.hospital.belen.client.citas.wrappers.EspecialidadWrapper;
import com.izune.hospital.belen.client.citas.wrappers.ProcedenciaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.ReferenciaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.TipoCitaWrapper;
import com.izune.hospital.belen.client.citas.wrappers.TipoPacienteWrapper;
import com.izune.hospital.belen.client.personas.controllers.PacienteController;
import com.izune.hospital.belen.client.personas.views.PacienteUI;
import com.izune.hospital.belen.client.personas.wrappers.PacienteWrapper;
import com.izune.hospital.belen.client.utils.views.SearchUI;
import com.izune.isabel.Isabel;
import com.izune.isabel.exceptions.InvalidTextFormat;
import com.izune.isabel.exceptions.InvalidTypeColumn;
import com.izune.isabel.exceptions.NotFoundColumnAnnotation;
import com.izune.isabel.exceptions.NotFoundDisplayValueAnnotation;
import com.izune.isabel.interfaces.IMaster;
import com.izune.isabel.utils.Message;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author aneyra
 */
@Component
public class CitaUI extends javax.swing.JDialog implements IMaster<CitaWrapper> {

    private static final long serialVersionUID = 2583153295501059644L;

    private CitaWrapper citaWrapper;

    @Autowired
    private SearchUI<EspecialidadWrapper> especialidades;

    @Autowired
    private SearchUI<PacienteWrapper> pacientes;

    @Autowired
    private SearchUI<ProcedenciaWrapper> procedencias;

    @Autowired
    private EspecialidadController especialidadController;

    @Autowired
    private ProcedenciaController procedenciaController;

    @Autowired
    private CitaController citaController;

    @Autowired
    private CronogramaController cronogramaController;

    @Autowired
    private PacienteController pacienteController;

    @Autowired
    private SearchUI<DisponibilidadWrapper> horarios;
    private boolean saved;

    /**
     * Creates new form CitaUI
     */
    public CitaUI() {
        super((java.awt.Frame) null, true);
        initComponents();
        setLocationRelativeTo(null);
        this.atajos();
        this.config();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelUI = new com.izune.isabel.ui.PanelUI();
        txtPaciente = new com.izune.isabel.ui.TextFieldUI();
        calFecha = new com.izune.isabel.ui.CalendarUI();
        btnDisponibilidad = new com.izune.isabel.ui.ButtonUI();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtMedico = new com.izune.isabel.ui.TextFieldUI();
        txtReferencia = new com.izune.isabel.ui.TextFieldUI();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cboTipoPaciente = new com.izune.isabel.ui.ComboBoxUI<>();
        txtNroDocumento = new com.izune.isabel.ui.TextFieldUI();
        txtEspecialidadId = new com.izune.isabel.ui.TextFieldUI();
        jLabel8 = new javax.swing.JLabel();
        txtEspecialidad = new com.izune.isabel.ui.TextFieldUI();
        txtNroHistoria = new com.izune.isabel.ui.TextFieldUI();
        jLabel4 = new javax.swing.JLabel();
        txtProcedenciaId = new com.izune.isabel.ui.TextFieldUI();
        jLabel9 = new javax.swing.JLabel();
        txtProcedencia = new com.izune.isabel.ui.TextFieldUI();
        jLabel10 = new javax.swing.JLabel();
        cboTipoCita = new com.izune.isabel.ui.ComboBoxUI<>();
        calFechaReferencia = new com.izune.isabel.ui.CalendarUI();
        jLabel11 = new javax.swing.JLabel();
        btnCerrar = new com.izune.isabel.ui.ButtonUI();
        btnGrabar = new com.izune.isabel.ui.ButtonUI();
        btnBuscarPaciente = new com.izune.isabel.ui.ButtonUI();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);

        txtPaciente.setEnabled(false);
        txtPaciente.setRequired(true);

        calFecha.setEnabled(false);
        calFecha.setRequired(true);

        btnDisponibilidad.setText("Buscar disponibilidad [F3]");
        btnDisponibilidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDisponibilidadActionPerformed(evt);
            }
        });

        jLabel1.setText("Tipo paciente");

        jLabel5.setText("Médico");

        jLabel2.setText("Paciente");

        txtMedico.setEnabled(false);
        txtMedico.setRequired(true);

        txtReferencia.setRequired(true);
        txtReferencia.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtReferenciaFocusLost(evt);
            }
        });

        jLabel6.setText("Nro Referencia");

        jLabel3.setText("Especialidad");

        jLabel7.setText("Documento de identidad");

        cboTipoPaciente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboTipoPacienteItemStateChanged(evt);
            }
        });

        txtNroDocumento.setEditable(false);
        txtNroDocumento.setEnabled(false);
        txtNroDocumento.setRequired(true);

        txtEspecialidadId.setRequired(true);
        txtEspecialidadId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtEspecialidadIdFocusLost(evt);
            }
        });
        txtEspecialidadId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtEspecialidadIdKeyPressed(evt);
            }
        });

        jLabel8.setText("Nro historia clínica");

        txtEspecialidad.setEnabled(false);

        txtNroHistoria.setEditable(false);
        txtNroHistoria.setEnabled(false);
        txtNroHistoria.setRequired(true);
        txtNroHistoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNroHistoriaKeyPressed(evt);
            }
        });

        jLabel4.setText("Fecha");

        txtProcedenciaId.setRequired(true);
        txtProcedenciaId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtProcedenciaIdFocusLost(evt);
            }
        });
        txtProcedenciaId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtProcedenciaIdKeyPressed(evt);
            }
        });

        jLabel9.setText("Lugar de procedencia");

        txtProcedencia.setEnabled(false);

        jLabel10.setText("Tipo cita");

        cboTipoCita.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboTipoCitaItemStateChanged(evt);
            }
        });

        calFechaReferencia.setEnabled(false);
        calFechaReferencia.setRequired(true);

        jLabel11.setText("Fecha referencia");

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        btnGrabar.setText("Grabar");
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

        btnBuscarPaciente.setText("Buscar Historia [F4]");
        btnBuscarPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPacienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelUILayout = new javax.swing.GroupLayout(panelUI);
        panelUI.setLayout(panelUILayout);
        panelUILayout.setHorizontalGroup(
            panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUILayout.createSequentialGroup()
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelUILayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel6)
                            .addComponent(jLabel1)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panelUILayout.createSequentialGroup()
                                .addComponent(txtProcedenciaId, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtProcedencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panelUILayout.createSequentialGroup()
                                .addComponent(txtNroHistoria, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelUILayout.createSequentialGroup()
                                .addComponent(txtReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(calFechaReferencia, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelUILayout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addGap(140, 140, 140))))
                            .addComponent(cboTipoPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelUILayout.createSequentialGroup()
                                .addComponent(calFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnDisponibilidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelUILayout.createSequentialGroup()
                                .addComponent(txtEspecialidadId, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtEspecialidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(cboTipoCita, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelUILayout.createSequentialGroup()
                        .addContainerGap(460, Short.MAX_VALUE)
                        .addComponent(btnGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21))
            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelUILayout.setVerticalGroup(
            panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUILayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cboTipoCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtEspecialidadId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel4)
                    .addComponent(calFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDisponibilidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cboTipoPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel6)
                    .addComponent(txtReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(calFechaReferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtProcedenciaId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtProcedencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtNroHistoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtNroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(panelUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGrabar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(jLabel12))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelUI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelUI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboTipoCitaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboTipoCitaItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cboTipoCitaItemStateChanged

    private void txtProcedenciaIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProcedenciaIdKeyPressed
        if (KeyEvent.VK_ENTER == evt.getKeyCode()) {
            if (this.txtProcedenciaId.getText().isEmpty() || this.txtProcedenciaId.getText().equals("0")) {
                try {
                    procedencias.setTitle("Buscar lugar de procedencia");
                    procedencias.setSize(800, 400);
                    procedencias.setClazz(ProcedenciaWrapper.class);
                    procedencias.setData(procedenciaController.listaProcedencia());
                    procedencias.setVisible(true);
                    if (null != procedencias.getObject()) {
                        this.txtProcedenciaId.setValueInt(procedencias.getObject().getId());
                        this.txtProcedencia.setText(procedencias.getObject().getNombre());
                    }
                } catch (InvalidTextFormat | InvalidTypeColumn | NotFoundColumnAnnotation | IllegalAccessException | IllegalArgumentException e) {

                }
            }
        }
    }//GEN-LAST:event_txtProcedenciaIdKeyPressed

    private void txtProcedenciaIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtProcedenciaIdFocusLost
        try {
            if (!txtProcedenciaId.getText().isEmpty() && !this.txtProcedenciaId.getText().equals("0")) {

                ProcedenciaWrapper procedenciaWrapper = procedenciaController.obtenerProcedencia(
                        txtProcedenciaId.getValueInt());
                if (null != procedenciaWrapper) {
                    this.txtProcedencia.setText(procedenciaWrapper.getNombre());
                } else {
                    this.txtProcedenciaId.setText("");
                }
            }
        } catch (HttpClientErrorException e) {
            panelUI.setErrorMessage(e);
        } catch (InvalidTextFormat e) {
        }
    }//GEN-LAST:event_txtProcedenciaIdFocusLost

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGrabarActionPerformed
        if (this.panelUI.isVerify()) {
            if (this.isVerify()) {
                try {
                    citaWrapper.setEspecialidadId(txtEspecialidadId.getValueInt());
                    citaWrapper.setFecha(calFecha.getCalendar());
                    citaWrapper.setTipoPacienteId(cboTipoPaciente.getValue().getId());
                    citaWrapper.setTipoCitaId(cboTipoCita.getValue().getId());
                    citaWrapper.setReferencia(txtReferencia.getText());
                    if (0 == this.cboTipoPaciente.getSelectedIndex()) {
                        citaWrapper.setProcedenciaId(txtProcedenciaId.getValueInt());
                        citaWrapper.setFechaReferencia(calFechaReferencia.getCalendar());
                    }
                    citaController.grabarCita(citaWrapper);
                    this.dispose();
                } catch (HttpClientErrorException e) {
                    this.panelUI.setErrorMessage(e);
                } catch (InvalidTextFormat e) {
                }

            }
        }
    }//GEN-LAST:event_btnGrabarActionPerformed

    private void txtNroHistoriaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNroHistoriaKeyPressed

    }//GEN-LAST:event_txtNroHistoriaKeyPressed

    private void txtEspecialidadIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEspecialidadIdKeyPressed
        if (KeyEvent.VK_ENTER == evt.getKeyCode()) {
            if (this.txtEspecialidadId.getText().isEmpty() || this.txtEspecialidadId.getText().equals("0")) {
                try {
                    especialidades.setTitle("Buscar Especialidad");
                    especialidades.setSize(500, 300);
                    especialidades.setClazz(EspecialidadWrapper.class);
                    especialidades.setData(especialidadController.listaEspecialidades());
                    especialidades.setVisible(true);
                    if (null != especialidades.getObject()) {
                        this.txtEspecialidadId.setValueInt(especialidades.getObject().getId());
                        this.txtEspecialidad.setText(especialidades.getObject().getNombre());
                    }
                } catch (HttpClientErrorException e) {
                    this.panelUI.setErrorMessage(e);
                } catch (InvalidTextFormat | InvalidTypeColumn | NotFoundColumnAnnotation | IllegalAccessException | IllegalArgumentException e) {

                }
            }
        }
    }//GEN-LAST:event_txtEspecialidadIdKeyPressed

    private void txtEspecialidadIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtEspecialidadIdFocusLost
        try {
            if (!txtEspecialidadId.getText().isEmpty() && !this.txtEspecialidadId.getText().equals("0")) {

                EspecialidadWrapper especialidadWrapper = especialidadController.obtenerEspecialidad(
                        txtEspecialidadId.getValueInt());
                if (null != especialidadWrapper) {
                    this.txtEspecialidad.setText(especialidadWrapper.getNombre());
                } else {
                    this.txtEspecialidadId.setText("");
                }
            }
        } catch (HttpClientErrorException e) {
            this.panelUI.setErrorMessage(e);
        } catch (InvalidTextFormat e) {
        }
    }//GEN-LAST:event_txtEspecialidadIdFocusLost

    private void cboTipoPacienteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboTipoPacienteItemStateChanged
        if (cboTipoPaciente.getSelectedIndex() > 0) {
            this.txtReferencia.setText("");
            this.calFechaReferencia.setDate(null);
            this.txtReferencia.setEnabled(false);
            this.calFechaReferencia.setEnabled(false);
            this.txtProcedenciaId.setEnabled(false);
        } else {
            this.txtReferencia.setEnabled(true);
            this.calFechaReferencia.setEnabled(true);
            this.txtProcedenciaId.setEnabled(true);
        }
    }//GEN-LAST:event_cboTipoPacienteItemStateChanged

    private void btnDisponibilidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDisponibilidadActionPerformed
        try {
            int especialidadId = this.txtEspecialidadId.getValueInt();
            if (0 != especialidadId) {
                horarios.setTitle("Diponibilidad médica");
                horarios.setSize(700, 400);
                horarios.setClazz(DisponibilidadWrapper.class);
                horarios.setData(cronogramaController.obtenerHorario(especialidadId));
                horarios.setVisible(true);
                if (null != horarios.getObject()) {
                    this.calFecha.setCalendar(horarios.getObject().getFecha());
                    this.txtMedico.setText(horarios.getObject().getMedico());
                    this.citaWrapper.setCronogramaId(horarios.getObject().getId());
                    this.citaWrapper.setMedicoId(horarios.getObject().getMedicoId());

                    this.cboTipoPaciente.requestFocus();
                }

            }
        } catch (InvalidTextFormat | InvalidTypeColumn | NotFoundColumnAnnotation | IllegalAccessException | IllegalArgumentException | HttpClientErrorException e) {

        }
    }//GEN-LAST:event_btnDisponibilidadActionPerformed

    private void txtReferenciaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtReferenciaFocusLost
        String numero = this.txtReferencia.getText();
        if (!numero.isEmpty()) {
            try {
                ReferenciaWrapper referenciaWrapper = citaController.obtenerRefrencia(numero);
                if (referenciaWrapper.isStatus()) {
                    switch (referenciaWrapper.getEstado()) {
                        case 0:
                            Message.warning("Número de referencia esta vencida");
                            break;
                        case 2:
                            Message.danger("No puede usar esta referencia paciente fue operado");
                            this.txtReferencia.setText("");
                            this.txtProcedenciaId.setText("");
                            this.txtProcedencia.setText("");
                            this.txtPaciente.setText("");
                            this.txtNroHistoria.setText("");
                            this.txtNroDocumento.setText("");
                            this.calFechaReferencia.setDate(null);
                            this.citaWrapper.setPacienteId(0);
                            this.citaWrapper.setProcedenciaId(0);
                            this.citaWrapper.setReferencia("");
                            this.citaWrapper.setReferenciaId(0);
                            this.btnBuscarPaciente.setEnabled(true);
                            this.txtProcedenciaId.setEnabled(true);
                            return;
                    }

                    this.txtProcedenciaId.setText(String.valueOf(referenciaWrapper
                            .getProcedenciaId()));
                    this.txtProcedencia.setText(referenciaWrapper.getProcedencia());
                    this.txtPaciente.setText(referenciaWrapper.getPaciente());
                    this.calFechaReferencia.setCalendar(referenciaWrapper.getFechaReferencia());
                    this.citaWrapper.setPacienteId(referenciaWrapper.getPacienteId());
                    this.citaWrapper.setProcedenciaId(referenciaWrapper.getProcedenciaId());
                    this.citaWrapper.setReferencia(referenciaWrapper.getNumero());
                    this.citaWrapper.setReferenciaId(referenciaWrapper.getId());
                    this.txtNroHistoria.setText(referenciaWrapper.getNumeroHistoria());
                    this.txtNroDocumento.setText(referenciaWrapper.getNumeroDocumento());
                    this.btnBuscarPaciente.setEnabled(false);
                    this.txtProcedenciaId.setEnabled(false);
                } else {
                    this.btnBuscarPaciente.setEnabled(true);
                    this.txtNroHistoria.setText("");
                    this.txtNroDocumento.setText("");
                    this.txtPaciente.setText("");

                }

            } catch (HttpClientErrorException e) {
            }

        }
    }//GEN-LAST:event_txtReferenciaFocusLost

    private void btnBuscarPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarPacienteActionPerformed

        try {
            pacientes.setTitle("Buscar Pacientes");
            pacientes.setSize(900, 400);
            pacientes.setClazz(PacienteWrapper.class);
            pacientes.setData(pacienteController.listarPacientes());
            pacientes.setUIClass(PacienteUI.class);
            pacientes.setVisible(true);
            if (null != pacientes.getObject()) {
                this.txtNroHistoria.setText(pacientes.getObject().getNumeroHistoria());
                this.txtNroDocumento.setText(pacientes.getObject().getNumeroDocumento());
                this.txtPaciente.setText(String.format("%s %s %s",
                        pacientes.getObject().getApellidoPaterno(),
                        pacientes.getObject().getApellidoMaterno(),
                        pacientes.getObject().getNombre()));
                citaWrapper.setPacienteId(pacientes.getObject().getId());
                this.btnGrabar.requestFocus();
            } else {
                this.txtNroHistoria.setText("");
                this.txtNroDocumento.setText("");
                this.txtPaciente.setText("");
                citaWrapper.setPacienteId(0);
            }
        } catch (InvalidTypeColumn | NotFoundColumnAnnotation
                | IllegalAccessException | IllegalArgumentException
                | HttpClientErrorException | InstantiationException
                | ParseException e) {

        }


    }//GEN-LAST:event_btnBuscarPacienteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.izune.isabel.ui.ButtonUI btnBuscarPaciente;
    private com.izune.isabel.ui.ButtonUI btnCerrar;
    private com.izune.isabel.ui.ButtonUI btnDisponibilidad;
    private com.izune.isabel.ui.ButtonUI btnGrabar;
    private com.izune.isabel.ui.CalendarUI calFecha;
    private com.izune.isabel.ui.CalendarUI calFechaReferencia;
    private com.izune.isabel.ui.ComboBoxUI<TipoCitaWrapper> cboTipoCita;
    private com.izune.isabel.ui.ComboBoxUI<TipoPacienteWrapper> cboTipoPaciente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private com.izune.isabel.ui.PanelUI panelUI;
    private com.izune.isabel.ui.TextFieldUI txtEspecialidad;
    private com.izune.isabel.ui.TextFieldUI txtEspecialidadId;
    private com.izune.isabel.ui.TextFieldUI txtMedico;
    private com.izune.isabel.ui.TextFieldUI txtNroDocumento;
    private com.izune.isabel.ui.TextFieldUI txtNroHistoria;
    private com.izune.isabel.ui.TextFieldUI txtPaciente;
    private com.izune.isabel.ui.TextFieldUI txtProcedencia;
    private com.izune.isabel.ui.TextFieldUI txtProcedenciaId;
    private com.izune.isabel.ui.TextFieldUI txtReferencia;
    // End of variables declaration//GEN-END:variables

    private void atajos() {
        this.rootPane.registerKeyboardAction(e -> {
            btnGrabar.doClick();
        }, KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_MASK), JComponent.WHEN_IN_FOCUSED_WINDOW);

        this.rootPane.registerKeyboardAction(e -> {
            btnCerrar.doClick();
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    private void config() {
        this.cboTipoCita.setClazz(TipoCitaWrapper.class);
        this.cboTipoPaciente.setClazz(TipoPacienteWrapper.class);
        this.txtEspecialidadId.setFormat(Isabel.NUMBER, true);
        this.txtProcedenciaId.setFormat(Isabel.NUMBER, true);
    }

    private void cargarCombos() {
        try {
            cboTipoPaciente.setItems(citaController.obtenerTiposPaciente());
            cboTipoCita.setItems(citaController.obtenerTiposCita());
        } catch (NotFoundDisplayValueAnnotation | IllegalAccessException | IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void setObject(CitaWrapper object) {
        this.citaWrapper = object;
    }

    @Override
    public CitaWrapper getObject() {

        return this.citaWrapper;
    }

    @Override
    public void setEdit(boolean edit) {
        this.panelUI.reset();
        this.cargarCombos();
        this.btnBuscarPaciente.setEnabled(true);
        if (edit) {
            this.cargarDatos();
            this.setTitle("Editar cita");
        } else {
            this.citaWrapper = new CitaWrapper();
            this.setTitle("Nueva cita");
        }
    }

    @Override
    public boolean isVerify() {
        if (citaWrapper.getPacienteId() == citaWrapper.getMedicoId()) {
            this.panelUI.setErrorMessage("El paciente no puede ser el médico seleccionado");
            return false;
        }

        if (calFecha.getDate() == null) {
            this.panelUI.setErrorMessage("Selecione la disponibilidad horaria");
            return false;
        }
        return true;
    }

    @Override
    public boolean isSaved() {

        return this.saved;
    }

    private void cargarDatos() {
        try {
            this.cboTipoCita.setValue(citaWrapper.getTipoCitaId());
            this.txtEspecialidadId.setValueInt(citaWrapper.getEspecialidadId());
            this.txtEspecialidad.setText(citaWrapper.getEspecialidad());
            this.calFecha.setCalendar(citaWrapper.getFecha());
            this.txtMedico.setText(citaWrapper.getMedico());
            this.cboTipoPaciente.setValue(citaWrapper.getTipoPacienteId());
            if (0 == this.cboTipoPaciente.getSelectedIndex()) {
                this.txtReferencia.setText(citaWrapper.getReferencia());
                this.calFechaReferencia.setCalendar(citaWrapper.getFechaReferencia());
                this.txtProcedenciaId.setValueInt(citaWrapper.getProcedenciaId());
                this.txtProcedencia.setText(citaWrapper.getProcedencia());
            }

            this.txtNroHistoria.setText(citaWrapper.getNumeroHistoria());
            this.txtNroDocumento.setText(citaWrapper.getNumeroDocumento());
            this.txtPaciente.setText(citaWrapper.getPaciente());
        } catch (InvalidTextFormat | IllegalAccessException | IllegalArgumentException | NoSuchFieldException e) {
        }
    }

}
